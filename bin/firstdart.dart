import 'dart:io';

void main() {
  print('Enter your name:');
  String? name = stdin.readLineSync();
  print('Enter your age:');
  String? a = stdin.readLineSync();
  int age = int.parse(a!);
  print('Hello, $name!');
  if (age >= 18) {
    print('คุณ $name สามารถเลือกตั้งได้');
    //print('you are eligible for voting');
  } else {
    print('คุณ $name ยังเป็นเยาวชน');
  }
}